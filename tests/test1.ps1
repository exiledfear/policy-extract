﻿# Test 1
#
# Simple test to loop through directory and grab all the file names.



#define the directory we are going to loop through
#for now it will be a relative path to the current script.
$dir = (Get-Item $PSScriptRoot).Parent.FullName + "\XML_Files";
Write-Host $dir;

foreach ($file in Get-ChildItem $dir) {

    $fullPath = $dir + "\" + $file;
    #do Some processing here.

    #Write-Host $fullPath;
    Try {
        #Write-Host "Parsed '"$file"' succeeded";
        [xml]$xml = Get-Content -Path $fullPath;
    } Catch {
        Write-Host "Seems like the file '"$file"' contains invalid XML";
        #Write-Host $_.Exception.Message;
    }



}