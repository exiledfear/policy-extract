﻿# Test 3
#
# Next step Validate is XML .extension
#
# Assume that we are in the "tests" directory of the project And we can go up the directry tree
# One level to find the XML_Files. We change to a test3 folder in the XML_Files to test our regular expression to only match on .xml
#


#define the directory we are going to loop through
#for now it will be a relative path to the current script.
$dir = (Get-Item $PSScriptRoot).Parent.FullName + "\XML_Files\test3";




foreach ($file in Get-ChildItem $dir) {

    $fullPath = $dir + "\" + $file;
    #do Some processing here.

    #Write-Host $fullPath;
  

    # Validate is XML file.
    if (!($file -match '.xml')) {
        continue;
    }



    # Validate is in valid DOC ID



    # Try to parse the XML inside the file.
    Try {
      
        [xml]$xml = Get-Content -Path $fullPath;
        Write-Host "Parsed '"$file"' succeeded. Seems to be valid XML";
    } Catch {
        Write-Host "Seems like the file '"$file"' contains invalid XML";
        #Write-Host $_.Exception.Message;
    }



}