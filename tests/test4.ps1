﻿# Test 4
#
# Next step validate filename against valid DOC ID's. Adding array here to store the IDs.
#
# Assume that we are in the "tests" directory of the project And we can go up the directry tree
# One level to find the XML_Files. Used a sub directory 'test4' to validate valid docIDs.
#


#define the directory we are going to loop through
#for now it will be a relative path to the current script.
$dir = (Get-Item $PSScriptRoot).Parent.FullName + "\XML_Files\test4";

# Array of valid Document IDs. Put new ones in here to validate on filename.
$docIDs = "ACR001","ACR014","DAC001","DLAI00","CNAI00";


foreach ($file in Get-ChildItem $dir) {

    $fullPath = $dir + "\" + $file;
    #do Some processing here.

    #Write-Host $fullPath;
  

    # Validate is XML file.
    # If it doesn't match the .xml extension at the end then continue without going further.
    if (!($file -match '.xml$')) {
        continue;
    }


    $inDocIDList = $false;
    # Validate is in valid DOC ID
    # It has to  match at least one of the items in the $docIDs array.
    ForEach ($ID in $docIDs) {
          if ($file -match $ID) {
            $inDocIDList = $true;
          }
    }

    # We will break from the current loop if the file is not in the valid docID list.
    if (!$inDocIDList) {
        Write-Host "Seems as though the file"$file" is not in the valid Doc ID list";
        continue;
    }


    # Try to parse the XML inside the file.
    Try {
      
        [xml]$xml = Get-Content -Path $fullPath;
        Write-Host "Parsed '"$file"' succeeded. Seems to be valid XML";
    } Catch {
        Write-Host "Seems like the file '"$file"' contains invalid XML";
        #Write-Host $_.Exception.Message;
        continue;
    }

    # At this point we should be able to use the $xml variable to grab the XML information.



}