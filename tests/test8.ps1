﻿# Test 8
#
# Test outputting the data to a CSV.
#
# Assume that we are in the "tests" directory of the project And we can go up the directry tree
# One level to find the XML_Files. 
#


#define the directory we are going to loop through
#for now it will be a relative path to the current script.
$dir = (Get-Item $PSScriptRoot).Parent.FullName + "\XML_Files";

# Array of valid Document IDs. Put new ones in here to validate on filename.
$docIDs = "ACR001","ACR014","DAC001","DLAI00","CNAI00";


# Verbose: Print out verbose explinations of wahts happening. Debug mode if you will.
$verbose = $false;

#empty array of policies that will be appended to after each loop.
$policies = @();


foreach ($file in Get-ChildItem $dir) {

    $fullPath = $dir + "\" + $file;

    # ---------------------------------------------------------------------------------------
    #      
    #                                        VALDATION
    #
    # ---------------------------------------------------------------------------------------

    # Validate is XML file.
    # If it doesn't match the .xml extension at the end then continue without going further.
    if (!($file -match '.xml$')) {
        continue;
    }


    $inDocIDList = $false;
    # Validate is in valid DOC ID
    # It has to  match at least one of the items in the $docIDs array.
    ForEach ($ID in $docIDs) {
          if ($file -match $ID) {
            $inDocIDList = $true;
          }
    }


    # Lets match again on a strict regex pattern to grab the policy #
    # Have to assign it to a variable to hide the print.
    $var = $file -match "(\w\d+)-(\d+)_\S{6}(\d{2})(\d{2})(\d{2})"
    
    $policyNumber = $Matches[1]
    $docID = $Matches[2]

    #make sure the policy number is set and validates through the regex above.
    if (!$policyNumber -and !$var -and !$docID) {
        if ($verbose) {
            Write-Host $file " Does not pass the file name validation";
        }
        continue;
    }


    # We will break from the current loop if the file is not in the valid docID list.
    if (!$inDocIDList) {
        if ($verbose) {
            Write-Host $file" is not in the valid Doc ID list";
        }
        continue;
    }

    # ---------------------------------------------------------------------------------------
    #      
    #                                        PARSE FILE
    #
    # ---------------------------------------------------------------------------------------


    # Try to parse the XML inside the file.
    Try {
      
        [xml]$xml = Get-Content -Path $fullPath;
        if ($verbose) {
            Write-Host $file" parse succeeded. Seems to be valid XML";
        }
    } Catch {
        if ($verbose) {
            Write-Host $file" contains invalid XML";
        }
        #Write-Host $_.Exception.Message;
        continue;
    }


    # ---------------------------------------------------------------------------------------
    #      
    #                                        BUILD OBJECT
    #
    # ---------------------------------------------------------------------------------------


    # At this point we should be able to use the $xml variable to grab the XML information.

    $fullName = $xml.Policy.InsuredInfo.FirstChild.FirstName + " " + $xml.Policy.InsuredInfo.FirstChild.LastName;


    # Requirement: If there are more than one recipients, use this filed for the second recipoient's name.
    if ($xml.Policy.InsuredInfo.ChildNodes.Count -gt 1) {
        $companyName = $xml.Policy.InsuredInfo.ChildNodes.Item(1).FirstName + " " + $xml.Policy.InsuredInfo.ChildNodes.Item(1).LastName;

    } else {
        $companyName = $fullName;
    }


    # Function to truncate output.
    function Output-truncate {
        #Write-Host $args[0].ToString().Length
        if ($args[0].ToString().Length -gt $args[1]) {
        
            return $args[0].ToString().Substring(0, $args[1]);
        }


       return $args[0].ToString();
    }
 

    


    $val = [pscustomobject]@{
            RecordType = 3 #required
            OrderID = Output-truncate $policyNumber"_"$docID 35 #required
            ClientID = ""
            TitleName = ""
            FirstName = Output-truncate $fullName 30
            LastName = Output-truncate $xml.Policy.InsuredInfo.FirstChild.LastName 40
            Title = ""
            CompanyName = Output-truncate $companyName 44
            AdditionalAddressInfo = Output-truncate $fullName 44
            AddressLine1 = Output-truncate $xml.Policy.MailingAddress.AddressLine1 44 #required
            AddressLine2 = Output-truncate $xml.Policy.MailingAddress.AddressLine2 44
            City = Output-truncate $xml.Policy.MailingAddress.City 25 #required
            Province = Output-truncate $xml.Policy.MailingAddress.ProvinceCode 2 #required
            PostalCode = Output-truncate $xml.Policy.MailingAddress.PostalCode 14 #required
            ContryCode = Output-truncate $xml.Policy.MailingAddress.Country 3
            ClientVoicePhone = ""
            ClientFaxNumber = ""
            ClientEmailAddress = ""
            Weight = 50
            Service = 908
            Length = ""
            Width = ""
            Height = ""
            DocumentIndicator = ""
            OversizeIndicator = ""
            DeliveryConfirmationIndicator = 1
            SignatureIndicator = 1
            Placeholder = ""
            DoNotSafeDropIndicator = 1
            CardForPickupIndicator = ""
            ProofofAgeRequired18 = ""
            ProofofAgeRequired19 = ""
            LeaveAtDoorIndicator = ""
            RegisteredIndicator = ""
            ProofofIdentityIndicator = ""
            Placeholder2 = ""
            DelivertoPostOffice = ""
            DestinationPostOfficeID = ""
            Placeholder3 = ""
            Placeholder4 = ""
            NotifyRecipient = ""
            InsuredAmount = ""
            CODValue = ""
            Placeholder5 = ""
            };
            
            Write-Host $val;

            $policies += $val;

}
    # ---------------------------------------------------------------------------------------
    #      
    #                                        BUILD OBJECT
    #
    # ---------------------------------------------------------------------------------------


$date = Get-Date -UFormat "%Y%m%d"
$outputFileName = (Get-Item $dir).Parent.FullName + "\PolicyExtract_" + $date + ".csv";

Write-Host "Exporting to "$outputFileName;

$policies | Export-Csv -Path $outputFileName;
