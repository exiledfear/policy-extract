﻿# Test 2
#
# Next step; validate as XML file. Try to parse the file file with built in capability.
#
# Assume that we are in the "tests" directory of the project And we can go up the directry tree
# One level to find the XML_Files.
#


#define the directory we are going to loop through
#for now it will be a relative path to the current script.
$dir = (Get-Item $PSScriptRoot).Parent.FullName + "\XML_Files";

foreach ($file in Get-ChildItem $dir) {

    $fullPath = $dir + "\" + $file;
    #do Some processing here.

    #Write-Host $fullPath;
  

    # Validate is XML file.


    # Validate is in valid DOC ID



    # Try to parse the XML inside the file.
    Try {
      
        [xml]$xml = Get-Content -Path $fullPath;
        Write-Host "Parsed '"$file"' succeeded. Seems to be valid XML";
    } Catch {
        Write-Host "Seems like the file '"$file"' contains invalid XML";
        #Write-Host $_.Exception.Message;
    }



}