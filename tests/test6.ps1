﻿# Test 6
#
# Need to grab the correct policy Number from the filename. Do so using regex match. This will happen after we've determined its a valid DocID.
#
# Assume that we are in the "tests" directory of the project And we can go up the directry tree
# One level to find the XML_Files. Use the test4 folder in the XML_Files. Because there are a couple of invalid files in there that won't match our
# new 
#


#define the directory we are going to loop through
#for now it will be a relative path to the current script.
$dir = (Get-Item $PSScriptRoot).Parent.FullName + "\XML_Files\test4";

# Array of valid Document IDs. Put new ones in here to validate on filename.
$docIDs = "ACR001","ACR014","DAC001","DLAI00","CNAI00";


#empty array of policies that will be appended to after each loop.
$policies = @();


foreach ($file in Get-ChildItem $dir) {

    $fullPath = $dir + "\" + $file;
    #do Some processing here.

    #Write-Host $fullPath;
  

    # Validate is XML file.
    # If it doesn't match the .xml extension at the end then continue without going further.
    if (!($file -match '.xml$')) {
        continue;
    }


    $inDocIDList = $false;
    # Validate is in valid DOC ID
    # It has to  match at least one of the items in the $docIDs array.
    ForEach ($ID in $docIDs) {
          if ($file -match $ID) {
            $inDocIDList = $true;
          }
    }


    # Lets match again on a strict regex pattern to grab the policy #
    # Have to assign it to a variable to hide the print.
    $var = $file -match "(\w\d+)-(\d+)_\S{6}(\d{2})(\d{2})(\d{2})"
    
    $policyNumber = $Matches[1]

    #make sure the policy number is set and validates through the regex above.
    if (!$policyNumber -and $var) {
        continue;
        }


    # We will break from the current loop if the file is not in the valid docID list.
    if (!$inDocIDList) {
        Write-Host "Seems as though the file"$file" is not in the valid Doc ID list";
        continue;
    }


    # Try to parse the XML inside the file.
    Try {
      
        [xml]$xml = Get-Content -Path $fullPath;
        Write-Host "Parsed '"$file"' succeeded. Seems to be valid XML";
    } Catch {
        Write-Host "Seems like the file '"$file"' contains invalid XML";
        #Write-Host $_.Exception.Message;
        continue;
    }

    # At this point we should be able to use the $xml variable to grab the XML information.

    $fullName = $xml.Policy.InsuredInfo.FirstChild.FirstName + " " + $xml.Policy.InsuredInfo.FirstChild.LastName;
    $val = [pscustomobject]@{
            RecordType = 3
            OrderID = $xml.Policy.PolicyNumber
            ClientID = ""
            TitleName = ""
            FirstName = $fullName
            LastName = $xml.Policy.InsuredInfo.FirstChild.LastName
            Title = ""
            CompanyName = $fullName
            AdditionalAddressInfo = $fullName
            AddressLine1 = $xml.Policy.MailingAddress.AddressLine1
            AddressLine2 = $xml.Policy.MailingAddress.AddressLine2
            City = $xml.Policy.MailingAddress.City
            Province = $xml.Policy.MailingAddress.ProvinceCode
            PostalCode = $xml.Policy.MailingAddress.PostalCode
            ContryCode = $xml.Policy.MailingAddress.Country
            ClientVoicePhone = ""
            ClientFaxNumber = ""
            ClientEmailAddress = ""
            Weight = 50
            Service = 908
            Length = ""
            Width = ""
            Height = ""
            DocumentIndicator = ""
            OversizeIndicator = ""
            DeliveryConfirmationIndicator = 1
            SignatureIndicator = 1
            Placeholder = ""
            DoNotSafeDropIndicator = 1
            CardForPickupIndicator = ""
            ProofofAgeRequired18 = ""
            ProofofAgeRequired19 = ""
            LeaveAtDoorIndicator = ""
            RegisteredIndicator = ""
            ProofofIdentityIndicator = ""
            Placeholder2 = ""
            DelivertoPostOffice = ""
            DestinationPostOfficeID = ""
            Placeholder3 = ""
            Placeholder4 = ""
            NotifyRecipient = ""
            InsuredAmount = ""
            CODValue = ""
            Placeholder5 = ""
            };
            
           # Write-Host $val;

}

#Write-Host $policies
