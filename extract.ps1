﻿# Extract.ps1
#
# 
# Script arguments 
# 
# Argument 1: Path to XML file folder.
# Argument 2: Debug Flag 
# Argument 3: Display Help block. Doesn't execute the program.
# Argument 4: include header row in CSV export.
# 
param($path, [Switch]$debug, [Switch]$help, [Switch]$includeHeaderRow)


# ---------------------------------------------------------------------------------------
#      
#                                PARAMETER VALIDATION
#
# ---------------------------------------------------------------------------------------


#Help block
if ($help) {

    Write-Host 
    "Simple XML extractor.

     Usage: extract.ps1 [-debug] [-path] [-help]
                
        - debug             Show debug output of the extractor. This is off by default.
        - path              Define where the XML files to extract are. By default it uses the path of the script.
        - includeHeaderRow  Have the CSV output include the header row containing data Column headings.
        - help              Display this information

                
    "
    exit
}


# If path is not defined then assume we are working from the script path.
# Script path has changed because I've moved the main script file up a folder.
if (!$path) {
    $dir = (Get-Item $PSScriptRoot).FullName + "\XML_Files";
} else {
    $dir = $path;
}

#If debug flag is set set the debugpreference to continue.
if ($debug) {
    $DebugPreference = "Continue";
}

# Include header row parameter validation. If flag not passed set to false. else true. Used at the end of script.
if (!$includeHeaderRow) {
    $includeHeaderRow = $false;
} else {
    $includeHeaderRow = $true;
}


# ---------------------------------------------------------------------------------------
#      
#                                SCRIPT SETUP
#
# ---------------------------------------------------------------------------------------


#define the directory we are going to loop through
#for now it will be a relative path to the current script.
#$dir = (Get-Item $path).Parent.FullName + "\XML_Files";

# Array of valid Document IDs. Put new ones in here to validate on filename.
$docIDs = "ACR001","ACR014","DAC001","DLAI00","CNAI00";


#empty array of policies that will be appended to after each loop.
$policies = @();

# count of files parsed and interpreted properly
$count = 0;

foreach ($file in Get-ChildItem $dir) {

    $fullPath = $dir + "\" + $file;

    # ---------------------------------------------------------------------------------------
    #      
    #                                        VALDATION
    #
    # ---------------------------------------------------------------------------------------

    # Validate is XML file.
    # If it doesn't match the .xml extension at the end then continue without going further.
    if (!($file -match '.xml$')) {
        continue;
    }


    $inDocIDList = $false;
    # Validate is in valid DOC ID
    # It has to  match at least one of the items in the $docIDs array.
    ForEach ($ID in $docIDs) {
          if ($file -match $ID) {
            $inDocIDList = $true;
          }
    }


    # Lets match again on a strict regex pattern to grab the policy #
    # Have to assign it to a variable to hide the print.
    $var = $file -match "(\w\d+)-(\d+)_\S{6}(\d{2})(\d{2})(\d{2})"
    
    $policyNumber = $Matches[1]
    $docID = $Matches[2]

    #make sure the policy number is set and validates through the regex above.
    if (!$policyNumber -and !$var -and !$docID) {
            Write-Debug "$file Does not pass the file name validation";
        continue;
    }


    # We will break from the current loop if the file is not in the valid docID list.
    if (!$inDocIDList) {
            Write-Debug "$file is not in the valid Doc ID list";
        continue;
    }

    # ---------------------------------------------------------------------------------------
    #      
    #                                        PARSE FILE
    #
    # ---------------------------------------------------------------------------------------


    # Try to parse the XML inside the file.
    Try {
      
        [xml]$xml = Get-Content -Path $fullPath;
            Write-Debug $file" parse succeeded. Seems to be valid XML";
    } Catch {
            Write-Debug $file" contains invalid XML";
        #Write-Debug $_.Exception.Message;
        continue;
    }


    # ---------------------------------------------------------------------------------------
    #      
    #                                        BUILD OBJECT
    #
    # ---------------------------------------------------------------------------------------


    # At this point we should be able to use the $xml variable to grab the XML information.

    $fullName = $xml.Policy.InsuredInfo.FirstChild.FirstName + " " + $xml.Policy.InsuredInfo.FirstChild.LastName;


    # Requirement: If there are more than one recipients, use this filed for the second recipoient's name.
    if ($xml.Policy.InsuredInfo.ChildNodes.Count -gt 1) {
        $companyName = $xml.Policy.InsuredInfo.ChildNodes.Item(1).FirstName + " " + $xml.Policy.InsuredInfo.ChildNodes.Item(1).LastName;

    } else {
        $companyName = $fullName;
    }


    # Function to truncate output.
    function Output-truncate {
        #Write-Debug $args[0].ToString().Length
        if ($args[0].ToString().Length -gt $args[1]) {
        
            return $args[0].ToString().Substring(0, $args[1]);
        }


       return $args[0].ToString();
    }
 

    #check that required fields are defined. Cannot be empty.
    $address1 = $xml.Policy.MailingAddress.AddressLine1;
    $city = $xml.Policy.MailingAddress.City;
    $province = $xml.Policy.MailingAddress.ProvinceCode;
    $postal = $xml.Policy.MailingAddress.PostalCode;
    $country = $xml.Policy.MailingAddress.Country;


    if (!$address1 -or !$city -or !$province -or !$postal) {
        Write-Debug "Looks like a required field in the file $file was not defined. Either Address, City, Province or Postal code is undefined. Therefore not extracting the information."
        continue;
    }


    # Check for valid postal code or zip code. Don't extract information if it doesn't validate.
    $isPostalCode = $postal -match "\w\d\w\s{0,1}\d\w\d";
    $isZipCode = $postal -match "^\d{5,9}$" -or $postal -match "^\d{5,9}-\d{4}$";


    if (!($isPostalCode -or $isZipCode)) {
        Write-Debug "$file does not contain a valid postal or zipe code. Skipping file";
        continue;
    }



    $val = [pscustomobject]@{
            RecordType = 3 #required
            OrderID = Output-truncate $policyNumber"_"$docID 35 #required This field will be defined always. otherwise wont get this far.
            ClientID = ""
            TitleName = ""
            FirstName = Output-truncate $fullName 30
            LastName = Output-truncate $xml.Policy.InsuredInfo.FirstChild.LastName 40
            Title = ""
            CompanyName = Output-truncate $companyName 44
            AdditionalAddressInfo = Output-truncate $fullName 44
            AddressLine1 = Output-truncate $address1 44 #required
            AddressLine2 = Output-truncate $xml.Policy.MailingAddress.AddressLine2 44
            City = Output-truncate $city 25 #required
            Province = Output-truncate $province 2 #required
            PostalCode = Output-truncate $postal 14 #required
            ContryCode = Output-truncate $country 3
            ClientVoicePhone = ""
            ClientFaxNumber = ""
            ClientEmailAddress = ""
            Weight = 50
            Service = 908
            Length = ""
            Width = ""
            Height = ""
            DocumentIndicator = ""
            OversizeIndicator = ""
            DeliveryConfirmationIndicator = 1
            SignatureIndicator = 1
            Placeholder = ""
            DoNotSafeDropIndicator = 1
            CardForPickupIndicator = ""
            ProofofAgeRequired18 = ""
            ProofofAgeRequired19 = ""
            LeaveAtDoorIndicator = ""
            RegisteredIndicator = ""
            ProofofIdentityIndicator = ""
            Placeholder2 = ""
            DelivertoPostOffice = ""
            DestinationPostOfficeID = ""
            Placeholder3 = ""
            Placeholder4 = ""
            NotifyRecipient = ""
            InsuredAmount = ""
            CODValue = ""
            Placeholder5 = ""
            };
            
            #Write-Debug $val;

            $policies += $val;
            $count++;

}


if ($count -gt 0) {
    Write-Host "$count xml file(s) extracted";
} else {
    Write-Host "Doesn't seem like there were any valid xml files found. No output file was created.";
    exit;
}

# ---------------------------------------------------------------------------------------
#      
#                                        BUILD OBJECT
#
# ---------------------------------------------------------------------------------------


$date = Get-Date -UFormat "%Y%m%d"


#lets define the output path of the CSVs into a folder called "output"
$outputFileName = (Get-Item $PSScriptRoot).FullName + "\output\PolicyExtract_" + $date + ".csv";
Write-Debug "Exporting to $outputFileName";

$policies | Export-Csv -Path $outputFileName -NoTypeInformation

# Remove header row
# Have to store the content in a variable otherwise file remained locked.
if (!$includeHeaderRow) {
    $content = Get-Content $outputFileName
    $content | select -Skip 1 | Set-Content $outputFileName
}