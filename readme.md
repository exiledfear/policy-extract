# Policy Extract

## How to run

There is an `extract.ps1` file in the root of this project. Open PowerShell and execute the script. Be sure you allow for script execution too. There are arguments that can be passed into the script. They are:

- -path [optional] Path to XML_File folder to parse. By default it assume's the script path and should work properly from there.
- -debug [optional] Pass an empty flag for debug to change the `$DebugPreference` variable in the PS script.
- -help [optional] Display this information in the shell
- -includeHeaderRow [optional] Include the header row for the CSV.

Once execution has been completed output files are stored in the `output` directory.

------

There is a full repository available at https://gitlab.com/exiledfear/policy-extract if you wish to see the version history of the project.

### Planning 

- Define the directory to grab the files from.
- Loop through the files contained in the directory.
- - Check for valid XML file. Via ext. Or mimetype
  - Regular expression check for or string search the document IDs:
  - - ACR001
    - ACR014
    - DAC001
    - DLAI00
    - CNAl00
  - If match then load file.
  - Load file XML into parser.
  - If file contains invalid XML or cannot be parsed display information to the screen.
  - If valid then use the parser to grab the information from the XML file accordingly.
    - Must check to make sure all required information exists before continuing.
    - All fields must be truncated according to layout requirements for csv.
  - Record the information in a CSV string format according to the layout specification.
    - Store the data in an object.
    - Store each object in an array
    - Pass the array to Output-CSV cmdlet
- Once the CSV is fully importable to excel its considered functionally done.
- Use parameters to optionally configure the script
  - First parameter to set the parse path.
  - Second parameter to set the debug flag
  - Third is a help flag
  - Forth is to include header in the CSV.

### Assumptions

- Assumed document IDs are 6 characters long.
- All scripts should be relative and should run from anywhere on a system. i.e. The script will work without configuration as long as its not moved.
- Cannot find most of the fields in the XML document. (mostly the default empty)
- Assuming "Conditionally Required" is the same as "Required" in Layout Requirements of specification.
- I don't own a copy of Office 2016 so I've downloaded and tested importing the CSV with a preview version of Office 2019
- All development and testing was done on Windows 10.
- Assuming if a required field isn't set don't try to extract anything else. Jump over the file.

### Testing

- Test files in the `test` directory include incremental tests done to complete the project. But given if I was to use a repository these tests could be seen through one file in the version history.
- Setting the input path of XML files helps a lot with having different packages of test sample input files.
- There are test directories in the `XML_Files` directory. 
  - `XML_Files\test3\*.txt` files used to test for valid .xml extension
  - `XML_Files\test4\P37733-9_DLA0102340.xml` to test for valid doc ID.
  - `XML_Files\test4\ACR001.xml` to test for valid filename that includes the policy # and doc#.
  - `XML_Files\test4\X62160-18_DAC001015024.xml` to test for valid XML.
  - `XML_Files\finaltest\Y62383-8_DAC001015022.xml` doesn't have an address line 1. Used that file to test for required fields. 
  - `XML_Files\finaltest\P37733-9_DLAI00102340.xml` doesn't have a valid postal code. Used to test zip/postal code validation as per requirements.
- I tested `extract.ps1` by downloading the script package onto another computer and running the script through Powershell. It prompted if I wanted to change the execution policy. 

## References

- https://www.vexasoft.com/blogs/powershell/7255220-powershell-tutorial-try-catch-finally-and-error-handling-in-powershell
- https://stackoverflow.com/questions/18032147/parsing-xml-using-powershell
- https://www.mssqltips.com/sqlservertip/2754/parsing-all-the-files-in-a-directory-using-powershell/
- https://kevinmarquette.github.io/2017-07-31-Powershell-regex-regular-expression/
- https://ss64.com/ps/syntax-arrays.html
- https://ss64.com/ps/foreach.html
- https://mcpmag.com/articles/2017/06/08/creating-csv-files-with-powershell.aspx
- https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-date?view=powershell-6
- https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/write-debug?view=powershell-6

